# X-Match Server

## Project setup

```
npm install
```

### Get production files from

[X-Match Front-End](https://gitlab.com/ArkF3r/x-match-front-end/)

### Run Serve

```
npm test
```
