const { verifyUser } = require("./Login.js");
const { registerUser } = require("./Register.js");
const { getPassword } = require("./Password.js");
const { Manejador } = require("./XMatch/Manejador/Manejador.js");
const signIn = async (req, res) => {
  var { user, password } = req.body;
  var status = false;
  if (await verifyUser(user, password)) {
    status = true;
  }
  var data = {
    status,
    user
  };
  if (data.status) {
    req.session.user = user;
  }
  res.json(data);
};

const verifySignIn = (req, res) => {
  var status = false;
  if (req.session.user) {
    status = true;
  }
  var data = {
    status
  };
  res.json(data);
};

const signUp = async (req, res) => {
  var datos = req.body;
  var status = false;
  if (await registerUser(datos)) {
    req.session.user = datos.User;
    status = true;
  }
  res.json({ status });
};

const getCodesInformation = async (req, res) => {
  var { codigos } = req.body;
  console.log(codigos);
  let Manager = new Manejador();
  let codesInformation = await Manager.buscarProductos(codigos);
  res.json(codesInformation);
};

const getIncidencia = async (req, res) => {
  var { codigo } = req.body;
  var { año } = req.body;
  console.log(codigo,año)
  let Manager = new Manejador();
  let codesInformation = await Manager.Incidencias(codigo,año);
  res.json(codesInformation);
};

const recoverPassword = async (req, res) => {
  var { mail } = req.body;
  var status = await getPassword(mail);
  res.json(status);
};

module.exports = {
  signIn,
  verifySignIn,
  getIncidencia,
  signUp,
  getCodesInformation,
  recoverPassword
};
