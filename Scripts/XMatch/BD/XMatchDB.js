const mysql = require("mysql");
class XMatchDB {
  constructor() {
    this.connectionInformation = {
      host: "127.0.0.1", //"178.128.75.144",
      port: 3307,
      user: "root",
      password: "password",
      database: "XMatch"
    };
  }
  executeQuery(textQuery) {
    return new Promise(resolve => {
      let connection = mysql.createConnection(this.connectionInformation);
      connection.query(textQuery, (err, result, fields) => {
        connection.destroy();
        if (err) {
          resolve({ status: false, err });
        } else {
          resolve({
            status: true,
            result,
            fields
          });
        }
      });
    });
  }
  async buscarProducto(codigo) {
    let Query = `CALL buscaProducto("${codigo}")`;
    let result = await this.executeQuery(Query);
  }
}
