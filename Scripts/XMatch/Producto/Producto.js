const { executeQueryProducts } = require("../../database");
class Producto {
  constructor(Codigo) {
    this.Codigo = Codigo;
  }
  assignProductInformation(productInformation) {
    for (var Information of productInformation) {
      if (Information !== undefined) {
        if (Information.Tienda === "WalMart") {
          this.Nombre = Information.Nombre;
          this.Imagen = Information.Imagen;
          this.WalMart = {
            //Nombre: Information.Nombre,
            Precio: Information.Precio,
            Tienda: Information.Tienda
          };
        } else if (Information.Tienda === "LaComer") {
          this.Nombre = this.Nombre || Information.Nombre;
          this.LaComer = {
            //Nombre: Information.Nombre,
            Precio: Information.Precio,
            Tienda: Information.Tienda
          };
        } else if (Information.Tienda === "Chedraui") {
          this.Nombre = this.Nombre || Information.Nombre;
          this.Imagen = this.Imagen || Information.Imagen;
          this.Chedraui = {
            //Nombre: Information.Nombre,
            Precio: Information.Precio,
            Tienda: Information.Tienda
          };
        }
      }
    }
  }
  async addToBD() {
    if (this.WalMart !== undefined) {
      await this.savePrice(this.WalMart);
    }
    if (this.Chedraui !== undefined) {
      await this.savePrice(this.Chedraui);
    }
    if (this.LaComer !== undefined) {
      await this.savePrice(this.LaComer);
    }
  }
  async savePrice(product) {
    var { Tienda, Precio } = product;

    try {
      await executeQueryProducts(
        `CALL AgregarPrecio( "${this.Codigo}", "${
          this.Nombre
        }", ${Precio}, "${Tienda}", "${this.Imagen}"  )`
      );
    } catch {
      console.log("Error al ejecutar");
    }
  }
}

module.exports = {
  Producto
};
