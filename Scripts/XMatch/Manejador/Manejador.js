const { BuscadorWalMart } = require("../Buscador/BuscadorWalMart");
const { BuscadorLaComer } = require("../Buscador/BuscadorLaComer");
const { BuscadorChedraui } = require("../Buscador/BuscadorChedraui");
const { executeQueryProducts } = require("../../database");
const { Producto } = require("../Producto/Producto");
class Manejador {
  constructor() {
    this.BWM = new BuscadorWalMart();
    this.BLC = new BuscadorLaComer();
    this.BCH = new BuscadorChedraui();
  }
  async buscarProductos(codigos) {
    let Productos = [];
    for (var codigo of codigos) {
      var producto = await this.buscarProducto(codigo);
      Productos.push(producto);
    }
    return Productos;
  }

  async buscarProducto(codigo) {
    var busquedaOnline = false;
    var resultados = await executeQueryProducts(
      `CALL BuscarCodigo("${codigo}")`
    );
    resultados = resultados.result[0];
    if (resultados === undefined) {
      var Walmart = await this.BWM.buscarProducto(codigo);
      var LaComer = await this.BLC.buscarProducto(codigo);
      var Chedraui = await this.BCH.buscarProducto(codigo);
      resultados = [Walmart, LaComer, Chedraui];
      busquedaOnline = true;
    }
    var producto = new Producto(codigo);
    producto.assignProductInformation(resultados);
    if (busquedaOnline) {
      await producto.addToBD();
    }
    return producto;
  }

  async generateReport() {
    return {
      status: true,
      report: undefined
    };
  }
  async Incidencias(codigo,año) {
    var resultados = await executeQueryProducts(
      `CALL obtenerIncidencia("${codigo}",${año});`
    );
    return resultados;
  }
}
module.exports = {
  Manejador
};
