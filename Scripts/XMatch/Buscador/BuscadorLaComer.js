const axios = require("axios");
const { Buscador } = require("./Buscador.js");
class BuscadorLaComer extends Buscador {
  constructor() {
    super();
    this.baseURL = "https://lacomer.buscador.amarello.com.mx";
    this.parameterURL = "/searchArtPrior?col=lacomer_2&succId=14&s=";
  }
  async buscarProducto(codigo) {
    console.log("Buscando: " + codigo);
    if (codigo.substring(0, 1) == 0) {
      codigo = "0" + codigo;
    }
    return await this.getProductInformation(codigo);
  }
  async getProductInformation(codigo) {
    var { data } = await axios.get(this.baseURL + this.parameterURL + codigo);
    console.log(this.baseURL + this.parameterURL + codigo);
    var producto = data.res[0];
    if (producto !== undefined) {
      var infoproducto = {
        Tienda: "LaComer",
        Nombre: producto.marDes.substring(0, 50),
        Precio: parseFloat(producto.artPrven),
        Codigo: codigo
      };
      return infoproducto;
    } else {
      return undefined;
    }
  }
}
module.exports = {
  BuscadorLaComer
};
// async function main() {
//   var a = new BuscadorLaComer();
//   console.log(await a.buscarProducto("7501055304745"));
// }

// main();
