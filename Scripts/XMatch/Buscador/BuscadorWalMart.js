const axios = require("axios");
const { Buscador } = require("./Buscador.js");
class BuscadorWalMart extends Buscador {
  constructor() {
    super();
    this.baseURL = "https://super.walmart.com.mx/";
    this.parameterURL =
      "/api/rest/model/atg/commerce/catalog/ProductCatalogActor/getSkuSummaryDetails?storeId=0000009999&upc=00";
    this.parameter2URL = "&skuId=00";
    this.parameterImage = "/images/product-images/img_large/00";
  }
  async buscarProducto(codigo) {
    console.log("Buscando: " + codigo);
    if (codigo.substring(0, 1) == 0) {
      codigo = "0" + codigo;
    }
    var infoproducto = await this.getProductInformation(codigo);
    if (infoproducto === undefined) {
      codigo = codigo.substring(0, codigo.length - 1);
      infoproducto = await this.getProductInformation(codigo);
    }
    return infoproducto;
  }
  async getProductInformation(codigo) {
    var { data } = await axios.get(
      this.baseURL + this.parameterURL + codigo + this.parameter2URL + codigo
    );
    var producto = data;
    if (producto.codeMessage !== "-1") {
      var infoproducto = {
        Tienda: "WalMart",
        Nombre: producto.skuDisplayNameText.substring(0, 50),
        Precio: parseFloat(producto.specialPrice),
        Codigo: codigo,
        Imagen: this.baseURL + this.parameterImage + codigo + "L.jpg"
      };
      return infoproducto;
    } else {
      return undefined;
    }
  }
}

module.exports = {
  BuscadorWalMart
};

// async function main() {
//   var a = new BuscadorWalMart();
//   console.log(await a.buscarProducto("750100802028"));
// }

// main();
