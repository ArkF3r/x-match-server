const { BuscadorWalMart } = require("./BuscadorWalMart");
const { BuscadorLaComer } = require("./BuscadorLaComer");
const { BuscadorChedraui } = require("./BuscadorChedraui");
const { codes } = require("./codes");
const { executeQueryProducts } = require("../../database");

async function main() {
  var bWM = new BuscadorWalMart();
  var bLC = new BuscadorLaComer();
  var bCH = new BuscadorChedraui();
  for (var code of codes) {
    var walmart = await bWM.buscarProducto(code);
    var LaComer = await bLC.buscarProducto(code);
    var Chedraui = await bCH.buscarProducto(code);
    if (walmart !== undefined) {
      await savePrice(walmart, code);
    }
    if (Chedraui !== undefined) {
      await savePrice(Chedraui, code);
    }
    if (LaComer !== undefined) {
      await savePrice(LaComer, code);
    }
  }
}

async function savePrice(product, code) {
  var { Tienda, Nombre, Precio, Imagen } = product;
  try {
    await executeQueryProducts(
      `CALL AgregarPrecio( "${code}", "${Nombre}", ${Precio}, "${Tienda}", "${Imagen}"  )`
    );
  } catch {
    console.log("Error al ejecutar");
  }
}

main();
