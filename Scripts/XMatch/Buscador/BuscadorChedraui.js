const axios = require("axios");
const { Buscador } = require("./Buscador.js");
class BuscadorChedraui extends Buscador {
  constructor() {
    super();
    this.baseURL = "https://www.chedraui.com.mx/";
    this.parameterURL = "/search/autocomplete/MobileSearchBox?term=";
  }
  async buscarProducto(codigo) {
    console.log("Buscando: " + codigo);
    //codigo = codigo.substring(0, codigo.length - 1);
    if (codigo.substring(0, 1) == 0) {
      codigo = "0" + codigo;
    }
    var infoproducto = await this.getProductInformation(codigo);
    if (infoproducto === undefined) {
      codigo = codigo.substring(0, codigo.length - 1);
      infoproducto = await this.getProductInformation(codigo);
    }
    return infoproducto;
  }
  async getProductInformation(codigo) {
    var { data } = await axios.get(this.baseURL + this.parameterURL + codigo);
    var producto = data.products[0];
    if (producto !== undefined) {
      var infoproducto = {
        Tienda: "Chedraui",
        Nombre: producto.name.substring(0, 50),
        Precio: producto.price.value,
        Codigo: codigo
      };
      try {
        infoproducto.Imagen = this.baseURL + producto.images[0].url;
      } catch {}
      return infoproducto;
    } else {
      return undefined;
    }
  }
}

module.exports = {
  BuscadorChedraui
};
// async function main() {
//   var a = new BuscadorChedraui();
//   console.log(await a.buscarProducto("750100802023"));
// }

// main();
