const { BuscadorWalMart } = require("./BuscadorWalMart");
const { BuscadorLaComer } = require("./BuscadorLaComer");
const { BuscadorChedraui } = require("./BuscadorChedraui");

async function main() {
  var bWM = new BuscadorWalMart();
  var bLC = new BuscadorLaComer();
  var bCH = new BuscadorChedraui();
  console.log(await bWM.buscarProducto("7506306215047")); //750100802028 // 750105534990
  console.log(await bLC.buscarProducto("7506306215047"));
  console.log(await bCH.buscarProducto("7506306215047")); //750100330980 // 750102420324

  console.log(await bWM.buscarProducto("750100802028")); //750100802028 // 750105534990
  console.log(await bLC.buscarProducto("750100802028"));
  console.log(await bCH.buscarProducto("750100802028"));

  console.log(await bWM.buscarProducto("750105534990")); //750100802028 // 750105534990
  console.log(await bLC.buscarProducto("750105534990"));
  console.log(await bCH.buscarProducto("750105534990"));
}

main();
