class Buscador {
  constructor() {
    if (new.target === Buscador) {
      throw new TypeError("Cannot construct Abstract instances directly");
    }
    if (this.buscarProducto === undefined) {
      // or maybe test typeof this.method === "function"
      throw new TypeError("Must override method");
    }
  }
}

module.exports = {
  Buscador
};
