const { executeQueryUser } = require("./database.js");

const registerUser = async datos => {
  var { RFC, Marca, User, Mail, Pass } = datos;
  try {
    var result = await executeQueryUser(
      `CALL addUser("${RFC}", "${Marca}", "${Mail}", "${User}", "${Pass}" )`
    );
    var { Status } = result.result[0][0];
    return Status;
  } catch (err) {
    console.log(err);
    return false;
  }
};

module.exports = {
  registerUser
};
