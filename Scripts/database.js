const mysql = require("mysql");
const databaseProducts = {
  host: "47.254.93.16", //"127.0.0.1",
  port: 3306, //3307,
  user: "user", //"root",
  password: "password",
  database: "XMatch"
};
const databaseUser = {
  host: "47.254.93.16", //"127.0.0.1",
  port: 3306, //3307,
  user: "user", //"root",
  password: "password",
  database: "XMatchUsers"
};

executeQueryProducts = function(textQuery) {
  return new Promise((resolve, reject) => {
    let connection = mysql.createConnection(databaseProducts);
    connection.query(textQuery, (err, result, fields) => {
      if (err) {
        connection.destroy();
        return reject(err);
      } else {
        connection.destroy();
        resolve({
          result,
          fields
        });
      }
    });
  });
};

executeQueryUser = function(textQuery) {
  return new Promise((resolve, reject) => {
    let connection = mysql.createConnection(databaseUser);
    connection.query(textQuery, (err, result, fields) => {
      if (err) {
        connection.destroy();
        return reject(err);
      } else {
        connection.destroy();
        resolve({
          result,
          fields
        });
      }
    });
  });
};

module.exports = { executeQueryProducts, executeQueryUser };
