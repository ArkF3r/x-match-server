const { executeQueryUser } = require("./database.js");

const verifyUser = async (user, pass) => {
  try {
    var result = await executeQueryUser(
      `CALL verifyUser("${user}", "${pass}")`
    );
    var { Status } = result.result[0][0];
    return Status;
  } catch (err) {
    console.log(err);
    return false;
  }
};

module.exports = {
  verifyUser
};
