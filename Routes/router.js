const express = require("express");
const functions = require(`../Scripts/functions.js`);
var router = express.Router();
// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});
router.post("/api/Login", (req, res) => {
  functions.signIn(req, res);
});

router.get("/api/Login", (req, res) => {
  functions.verifySignIn(req, res);
});

router.post("/api/Register", (req, res) => {
  functions.signUp(req, res);
});

router.post("/api/Codes", (req, res) => {
  functions.getCodesInformation(req, res);
});

router.post("/api/Password", (req, res) => {
  functions.recoverPassword(req, res);
});

router.post("/api/Stats/Code", (req, res) => {
  functions.getIncidencia(req, res);
});

module.exports = {
  router
};
