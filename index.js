const fs = require("fs");
const cors = require("cors");
const path = require("path");
const https = require("https");
const express = require("express");
const bodyParser = require("body-parser");
const session = require("express-session");
const { router } = require("./Routes/router");
const cookieParser = require("cookie-parser");
const history = require("connect-history-api-fallback");
let app = express();

const port = 8080;

app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//Manejo de cookies
app.use(cookieParser());

//Manejo de sesiones
app.use(
  session({
    secret: "ArkNeo",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  })
);

app.use(router);

//Manejo de router de vue app
app.use(history());
//Carpeta que contiene archivos estáticos
app.use(express.static("dist"));

https
  .createServer(
    {
      key: fs.readFileSync(path.join(__dirname + "/security/cert.key")),
      cert: fs.readFileSync(path.join(__dirname + "/security/cert.pem"))
    },
    app
  )
  .listen(port, () => {
    console.log("Listening..." + port);
  });

/*
  https
  .createServer(
    {
      key: fs.readFileSync(path.join(__dirname + "/security/cert.key")),
      cert: fs.readFileSync(path.join(__dirname + "/security/cert.pem"))
    },
    app
  )
  .listen(443);

http
  .createServer(function(req, res) {
    res.writeHead(301, {
      Location: "https://" + req.headers["host"] + req.url
    });
    res.end();
  })
  .listen(80);
  */
